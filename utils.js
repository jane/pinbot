import path from 'path';
import { platform } from 'process';
import { Constants } from 'eris';

export function filename(url) {
	const __filename = new URL(url).pathname;
	return path.basename(__filename, '.js');
}

export function dirname(url) {
	const __filename = new URL(url).pathname;
	const __dirname = path.dirname(__filename);
	return platform == 'win32' ? __dirname.slice(1) : __dirname;
}

export function getPerms(perms) {
	const result = [];
	for (const key in Constants.Permissions) {
		if (Constants.Permissions[key].toString() & perms) {
			result.push(key);
		}
	}
	return result;
}

export function hasPerms(perms, test) {
	let testPerms = getPerms(test);
	for (const tp of testPerms) {
		if (!perms.has(tp)) {
			return false;
		}
	}
  return true;
}
