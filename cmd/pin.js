import { CommandInitializer, Command, Event } from '../parser.js';
import Logger, { levels } from '../logger.js';
import { filename } from '../utils.js';

const initializer = new CommandInitializer();
const log = new Logger(filename(import.meta.url), global.ctx.log_level);

let activePins = {};

let defaultSettings = { threshold: 3, emoji: '📌' };

class PinEvent extends Event {
	event = 'messageReactionAdd';
	async func(ctx, args) {
		const msg = args[0];
		const fullMsg = await ctx.bot.getMessage(msg.channel.id, msg.id);
		const reaction = args[1];
		const reactor = args[2];
		log.trace(msg);
		log.trace(reaction);
		log.trace(reactor);
		let threshold = ctx.settings.getSetting(msg.channel.guild.id, 'pin', 'threshold');
		if (threshold == undefined) {
			threshold = defaultSettings.threshold;
			ctx.settings.setSetting(msg.channel.guild.id, 'pin', 'threshold', threshold);
		}
		let emoji = ctx.settings.getSetting(msg.channel.guild.id, 'pin', 'emoji');
		if (emoji == undefined) {
			emoji = defaultSettings.emoji;
			ctx.settings.setSetting(msg.channel.guild.id, 'pin', 'emoji', emoji);
		}
		if (reaction.name === emoji) {
			const fullReaction = await ctx.bot.getMessageReaction(msg.channel.id, msg.id, emoji);
			log.debug('reaction matched');
			log.trace(fullReaction);
			if (fullReaction.length >= threshold) {
				if (fullMsg.author.bot || fullReaction.filter((reaction) => reaction.id == fullMsg.author.id).length > 0) {
					if (activePins[fullMsg.id]) {
						activePins[fullMsg.id].delete();
					}
					fullMsg.pin().catch(async (err) => {
						let sn = await fullMsg.channel.createMessage('could not pin the message: ' + err.toString());
						if (err.toString().includes('Missing Permissions')) {
							sn.edit(
								'could not pin the message: ' +
									err.toString() +
									'\n(requires the manage messages or pin messages permission)'
							);
						}
					});
				} else {
					let text = '';
					text += fullMsg.author.mention + ', ';
					if (fullReaction.length == 1) {
						text += fullReaction[0].username + ' wants ';
					} else {
						for (let i = 0; i < fullReaction.length; i++) {
							text += (i == fullReaction.length - 1) ? 'and ' + fullReaction[i].username : fullReaction[i].username + ', ';
						}
						text += ' want ';
					}
					text += 'to pin your message. react with ' + emoji + ' to pin it.';
					let message = await fullMsg.channel.createMessage(text);
					activePins[fullMsg.id] = message;
				}
			}
		} else if (emoji.contains(reaction.name) && reaction.emoji(reaction.id)) {
			// good luck
		}
	}
}

initializer.addEvent(new PinEvent());

function settingsSubcommand(msg, args, ctx) {
	if (args[1] == undefined) {
		msg.channel.createMessage('not enough arguments given');
		return;
	}
	if (defaultSettings[args[1].trim()] == undefined) {
		msg.channel.createMessage(
			args[1] +
				' is not a valid setting.\navailable settings are:\n```\n' +
				Object.keys(defaultSettings).join('\n') +
				'\n```'
		);
		return;
	}
	if (args[2] == undefined) {
		let setting = ctx.settings.getSetting(msg.channel.guild.id, 'pin', args[1]);
		if (setting == undefined) {
			setting = defaultSettings[args[1]];
			ctx.settings.setSetting(msg.channel.guild.id, 'pin', args[1], defaultSettings[args[1]]);
		}
		msg.channel.createMessage(args[1] + ' is currently set to ' + setting);
	} else {
		ctx.settings.setSetting(msg.channel.guild.id, 'pin', args[1], args[2]);
		msg.channel.createMessage('set the value of ' + args[1] + ' to ' + args[2]);
	}
}

class PinCommand extends Command {
	name = 'pin';
	perms = 32;
	description = 'manage the pin module';
	helptext = '';
	func(msg, args, ctx) {
		switch (args[0]) {
			case 'settings':
			case 'setting':
				settingsSubcommand(msg, args, ctx);
				break;
			case undefined:
				msg.channel.createMessage('no arguments given');
				break;
			default:
				msg.channel.createMessage('unknown argument ' + args[0]);
		}
	}
}

initializer.addCommand(new PinCommand());

export default initializer;
