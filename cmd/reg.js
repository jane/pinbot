import { CommandInitializer, Command } from '../parser.js';
import Logger, { levels } from '../logger.js';
import { filename } from '../utils.js';

const initializer = new CommandInitializer();
const log = new Logger(filename(import.meta.url), global.ctx.log_level);

class PingCommand extends Command {
	name = 'ping';
	aliases = ['p'];
	func(msg, args, ctx) {
		msg.channel.createMessage('p').then((m) => {
			m.edit(
				`rtt: ${Math.floor(m.timestamp - msg.timestamp)}ms, gateway: ${
					ctx.bot.shards.get(ctx.bot.guildShardMap[ctx.bot.channelGuildMap[msg.channel.id]] || 0).latency
				}ms`
			);
		});
	}
}

initializer.addCommand(new PingCommand());

class RestartCommand extends Command {
	name = 'restart';
	func(msg, args, ctx) {
		msg.channel.createMessage('restarting.').then(() => {
			process.exit();
		});
	}
}

initializer.addCommand(new RestartCommand());

class EvalCommand extends Command {
	name = 'eval';
	whitelist = true;
	func(msg, args, ctx) {
		log.debug('evaluating ' + args[0]);
		let result;
		try {
			result = eval(args[0]);
		} catch (e) {
			result = e.stack;
		}
		log.debug('result is: \n' + result);
		if (String.valueOf(result).length <= 1999) {
			msg.channel.createMessage('```\n' + result + '```\n');
		} else {
			msg.channel.createMessage('result too long, see logs');
		}
	}
}

initializer.addCommand(new EvalCommand());

class HelpCommand extends Command {
	name = 'help';
	description = 'get help for commands';
	helptext = '```md\nhelp\nshows this text\n\nhelp <command>\nshows the helptext for <command>';
	func(msg, args, ctx) {}
}

initializer.addCommand(new HelpCommand());

export default initializer;
